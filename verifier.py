
# solution description:
# every number participates in 3 sectors that need to contain 9 different numbers (1 through 9),
# the sectors in total are 27 (9 rows, 9 columns, plus the 9 inner squares),
# each sector can be classified as a dictionary with keys 1 through 9,
# if one of the sectors happens to have a duplicate key the output will be false, otherwise true
#
# how do we identify the sectors?
# identifying rows and columns is trivial (we check the y and x index)
#
# identifying the inner square sector could be done based on a 2level 1 through 3 nested dictionary,
# we'll identify the corresponding x and y square position based on the result of floor(index / 3)
# visualization of the inner square store sectors:
#  -------------------
#  | 0-0 | 0-1 | 0-2 |
#  -------------------
#  | 1-0 | 1-1 | 1-2 |
#  -------------------
#  | 2-0 | 2-1 | 2-2 |
#  -------------------
#
# Note: we're not doing validation on the input numbers, we trust them to be a number > 0 and < 10

from math import floor

def verifier (o):
    # this is what the inner square store should look like
    squareStore = { y: { x: {} for x in range(0, 3) } for y in range(0, 3) }
    # we also prepare a store for the columns 
    colStores = { y: {} for y in range(0, 9) }

    for iy, row in enumerate(o):
      # we also prepare a store for the row 
      rowStore = {}
      # we conserve the innerSquare outerIndex, will be used afterwards during the squareStore check
      outerSqIndex = floor(iy / 3)
      for ix, x in enumerate(row):
    
        # we retrieve the innerSquare innerIndex
        innerSqIndex = floor(ix / 3)

        # we check the column, row and innerSquare store 
        if x in rowStore or x in colStores[ix] or x in squareStore[outerSqIndex][innerSqIndex]:
            return False

        # we add the value to all relevant stores
        colStores[ix][x] = True 
        rowStore[x] = True
        squareStore[outerSqIndex][innerSqIndex][x] = True

    return True


